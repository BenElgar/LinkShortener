<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Page;
use DB;

class PageController extends Controller
{
    public function show($code)
    {
        $page = Page::where('unique_code', $code)->first();

        if($page)
        {
            return redirect($page->url);
        }

        return ['error'];
    }

    public function create(Request $request)
    {
        $id = Page::max('id')+1;
        $data = [
            'url' => $request->input('url'),
            'unique_code' => base_convert($id, 10, 36),
        ];

        $page = Page::create($data);
        return $page;
    }

}
