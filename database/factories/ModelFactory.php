<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

//$factory->define(App\User::class, function (Faker\Generator $faker) {
    //return [
        //'name' => $faker->name,
        //'email' => $faker->email,
        //'password' => bcrypt(str_random(10)),
        //'remember_token' => str_random(10),
    //];
//});

$factory->define(App\Page::class, function (Faker\Generator $faker) {
    $code_characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
    $code_array = $faker->randomElements($code_characters, 4);
    $code = implode('', $code_array);
    return [
        'url'         => $faker->url,
        'unique_code' => $code,
    ];
});
