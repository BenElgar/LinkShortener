Link Shortener
==============

urlshort.com/0
urlshort.com/1
urlshort.com/3
...
urlshort.com/a
urlshort.com/b

urlshort.com/

Storing
-------
POST urlshort.com

{
    status: 201,
    data:
    {
        id: 1,
        short_url: urlshot.com/a1
    }
}


Status
------
GET urlshort.com/+a1

{
    status: 200,
    data:
    {
        id: 1,
        short_url:
    },
    sessions:
    [
        {
            id: 1,
            ip_address: 127.0.0.1,
            timestamp: 12:00 pm
        },
        ..
    ]
}


Pages
-----
id, url, unique_code, created_at, modified_at

Sessions
--------
id, page_id, timestamp, ip_address
